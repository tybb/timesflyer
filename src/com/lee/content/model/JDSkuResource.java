package com.lee.content.model;

/**
 * Author :Lee
 * Since :2018/8/23下午1:31
 * Desc :JDZNK Do What ?
 */
public class JDSkuResource {
    private int id;
    private String skuId;
    private String categorySeq;
    private String skuName;
    private String createDt;
    private String updateDt;
    private int state;

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public String getCreateDt() {
        return createDt;
    }

    public void setCreateDt(String createDt) {
        this.createDt = createDt;
    }

    public String getUpdateDt() {
        return updateDt;
    }

    public void setUpdateDt(String updateDt) {
        this.updateDt = updateDt;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getCategorySeq() {
        return categorySeq;
    }

    public void setCategorySeq(String categorySeq) {
        this.categorySeq = categorySeq;
    }
}