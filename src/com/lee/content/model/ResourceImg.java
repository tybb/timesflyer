package com.lee.content.model;

/**
 * Author :Lee
 * Since :2017/11/8上午12:22
 * Desc :ResourceImg Do What ?
 */
public class ResourceImg {


    private int id;
    private String location;
    private String createDt;
    private String updateDt;
    private int fromSeedId;
    private int status;
    private String title;
    private String describle;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCreateDt() {
        return createDt;
    }

    public void setCreateDt(String createDt) {
        this.createDt = createDt;
    }

    public String getUpdateDt() {
        return updateDt;
    }

    public void setUpdateDt(String updateDt) {
        this.updateDt = updateDt;
    }

    public int getFromSeedId() {
        return fromSeedId;
    }

    public void setFromSeedId(int fromSeedId) {
        this.fromSeedId = fromSeedId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescrible() {
        return describle;
    }

    public void setDescrible(String describle) {
        this.describle = describle;
    }
}