package com.lee.content.model;

import com.lee.system.http.HttpRequestContnet;
import com.lee.task.ITask;

import java.util.concurrent.LinkedBlockingQueue;

/**
 * Author :Lee
 * Since :2017/11/6下午7:48
 * Desc :SeedList Do What ?
 */
public class SeedList {

    private static LinkedBlockingQueue<ITask> list = null;

    static {
        list = new LinkedBlockingQueue();
    }

    public static Object put(ITask iTask) throws InterruptedException {
        list.put(iTask);
        return iTask;
    }

    public static LinkedBlockingQueue<ITask>  getSeedList()
    {
        return list;
    }
}