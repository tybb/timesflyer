package com.lee.content.model;

/**
 * Author :Lee
 * Since :2018/9/23下午5:32
 * Desc :JDCategories Do What ?
 */
public class JDCategories {

    private int id;
    private String categoriyName;
    private String categorySeq;
    private String url;
    private String createDt;
    private String updateDt;
    private int state;

    private String lastCatch;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategoriyName() {
        return categoriyName;
    }

    public void setCategoriyName(String categoriyName) {
        this.categoriyName = categoriyName;
    }

    public String getCategorySeq() {
        return categorySeq;
    }

    public void setCategorySeq(String categorySeq) {
        this.categorySeq = categorySeq;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCreateDt() {
        return createDt;
    }

    public void setCreateDt(String createDt) {
        this.createDt = createDt;
    }

    public String getUpdateDt() {
        return updateDt;
    }

    public void setUpdateDt(String updateDt) {
        this.updateDt = updateDt;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getLastCatch() {
        return lastCatch;
    }

    public void setLastCatch(String lastCatch) {
        this.lastCatch = lastCatch;
    }
}