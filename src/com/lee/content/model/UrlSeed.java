package com.lee.content.model;

import java.io.Serializable;

/**
 * Created by libia on 2016/2/1.
 */
public class UrlSeed implements Serializable {

        private int id;
        private String url;
        private String name;
        private String createDt;
        private String updateDt;
        private int isCatch;
        private int catchType;
        private int status ;

        public String getUrl() {
                return url;
        }

        public void setUrl(String url) {
                this.url = url;
        }

        public String getName() {
                return name;
        }

        public void setName(String name) {
                this.name = name;
        }

        public String getCreateDt() {
                return createDt;
        }

        public void setCreateDt(String createDt) {
                this.createDt = createDt;
        }

        public String getUpdateDt() {
                return updateDt;
        }

        public void setUpdateDt(String updateDt) {
                this.updateDt = updateDt;
        }

        public int getIsCatch() {
                return isCatch;
        }

        public void setIsCatch(int isCatch) {
                this.isCatch = isCatch;
        }

        public int getCatchType() {
                return catchType;
        }

        public void setCatchType(int catchType) {
                this.catchType = catchType;
        }

        public int getStatus() {
                return status;
        }

        public void setStatus(int status) {
                this.status = status;
        }

        public int getId() {
                return id;
        }

        public void setId(int id) {
                this.id = id;
        }
}
