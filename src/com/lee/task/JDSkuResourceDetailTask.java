package com.lee.task;

import com.google.common.base.Strings;
import com.lee.biz.impl.JDSkuDetailBiz;
import com.lee.biz.impl.JDSkuResourceBiz;
import com.lee.system.exception.MyHttpException;
import com.lee.system.http.HttpRequestContnet;
import com.lee.system.http.HttpRequestHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 * Author :Lee
 * Since :2018/9/23下午9:02
 * Desc :JDSkuResourceDetailTask Do What ?
 */
public class JDSkuResourceDetailTask implements ITask {


    private String resourceUrl = null;
    private Logger logger = LoggerFactory.getLogger(getClass());

    JDSkuDetailBiz biz = new JDSkuDetailBiz();

    public JDSkuResourceDetailTask(String resourceUrl ){
        this.resourceUrl = resourceUrl;

    }

    @Override
    public void execute() {
        HttpRequestContnet requestContnet = new HttpRequestContnet(resourceUrl);

        String pageResutl = null;
        try {
            pageResutl = new HttpRequestHandler().request(requestContnet);
        } catch (MyHttpException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (!Strings.isNullOrEmpty(pageResutl)) {


            biz.execute(pageResutl);
        }
    }

    public String getResourceUrl() {

        return resourceUrl;
    }

    public void setResourceUrl(String resourceUrl) {
        this.resourceUrl = resourceUrl;
    }
}