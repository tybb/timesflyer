package com.lee.task;

import com.google.common.base.Optional;
import com.google.common.base.Strings;
import com.lee.biz.impl.JDCategoriesBiz;
import com.lee.content.model.SeedList;
import com.lee.system.exception.MyHttpException;
import com.lee.system.http.HttpRequestContnet;
import com.lee.system.http.HttpRequestHandler;
import com.lee.system.thread.SearchThread;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;


/**
 * Author :Lee
 * Since :2018/8/28下午2:45
 * Desc :JDZnkDetailTask Do What ?
 */
public class JDTask implements ITask {

    private final String JD_CATEGORIES_URL = "https://www.jd.com/allSort.aspx";

    private Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public void execute() {

        JDCategoriesBiz jdCategoriesBiz = new JDCategoriesBiz();
        HttpRequestContnet requestContnet = new HttpRequestContnet(JD_CATEGORIES_URL);
        String pageResutl = null;
        try {
            pageResutl = new HttpRequestHandler().request(requestContnet);


            logger.info("JD 全品类 扫描 结果"+ pageResutl);

        } catch (MyHttpException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (!Strings.isNullOrEmpty(pageResutl)) {
            jdCategoriesBiz.execute(pageResutl);
        }


    }

}