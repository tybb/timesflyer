package com.lee.biz.impl;

import com.google.common.base.Objects;

import com.google.common.base.Strings;
import com.lee.biz.BaseBizInterface;
import com.lee.content.model.JDSkuResource;

import com.lee.content.model.SeedList;
import com.lee.system.jdbc.JdbcPool;
import com.lee.task.JDSkuResourceTask;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Author :Lee
 * Since :2018/8/23下午1:41
 * Desc :JDZnkBiz Do What ?
 */
public class JDSkuResourceBiz implements BaseBizInterface {
    private Logger logger = LoggerFactory.getLogger(getClass());

    private final String  JD_LIST_URL_PRE = "http://list.jd.com//";

    public boolean execute(String pageResult) {
        Pattern skuIdPattern = Pattern.compile("(\\d+):\\{(.|\\s)*?venderType(.|\\s)*?}");


        //<div class="p-name"><a target="_blank" title="" href="//item.jd.com/7123852.html"><em> 克来比（KLEBY） 电烧烤炉 家用无烟烧烤电烤盘 可拆卸电火锅烧烤一体鸳鸯锅 GM-8002 大号升级款 </em>

        //<a href="/list.html?cat=737,752,13118&amp;page=1&amp;sort=sort_rank_asc&amp;trans=1&amp;JL=6_0_0&amp;ms=3" class="curr">1</a>



//        <b class="pn-break "> …</b><a href="/list.html?cat=737,752,13118&amp;page=26&amp;sort=sort_rank_asc&amp;trans=1&amp;JL=6_0_0&amp;ms=3" class="">26</a>


        Pattern nextPagePattern = Pattern.compile("<a class=\"pn-next\" href=\"(.*?)\">下一页(.*?)</a>");
        Pattern maxPagePattern = Pattern.compile("<em>共<b>(\\d*?)</b>?");

        Matcher matcher = skuIdPattern.matcher(pageResult);

        String now = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());


        List l =  new ArrayList();
        String skuId = null;
        String skuName = null;

        String currentPage = null;
        String maxPage = null;


        String url = null;

        String nextPageUrl = null;



        Matcher matcher_nextpage  = nextPagePattern.matcher(pageResult);
        while (matcher_nextpage.find()) {
//            url = matcher_nextpage.group(1);
//            skuId = matcher_currentpage.group(2);
            nextPageUrl = matcher_nextpage.group(1);

        }


        Matcher matcher_maxpage  = maxPagePattern.matcher(pageResult);
        while (matcher_maxpage.find()) {
            maxPage = matcher_maxpage.group(1);
        }



        String catSeq = "";
        Matcher matcher_cate  = Pattern.compile("cat=(.*?)&").matcher(pageResult);
        if (matcher_cate.find()) {
            catSeq = matcher_cate.group(1);
        }



        SqlSession session = JdbcPool.buildSqlSession();
        try {
            while (matcher.find()) {
                skuId = matcher.group(1);

                Pattern skuNamePattern =  Pattern.compile("<a target=\"_blank\" title=\"\" href=\"//item.jd.com/"+skuId+"\\.html\">\\s*?<em>(.+?)</em>?");

                Matcher matcher_name  = skuNamePattern.matcher(pageResult);
                while (matcher_name.find()) {
                    skuName = matcher_name.group(1);
                }

                logger.info("****** ****** ****** ****** ******");
                logger.info("****** skuId : " + skuId  ) ;
                logger.info("skuName : " + skuName );
                logger.info("cateSeq : " + catSeq );
                logger.info("currentPage : " + currentPage );
                logger.info("nextPageUrl : " + nextPageUrl );
                logger.info("url :" + url );
                logger.info("****** ****** ****** ****** ******");


                /**
                 * 这里获取到当前页数 ，然后 对比 最大页数， 循环放入种子列表
                 */
                if ( null != nextPageUrl && !"".equals(nextPageUrl) )
                {

                         SeedList.put(new JDSkuResourceTask(nextPageUrl));

                }




                if (!Objects.equal("1", skuId) && session.selectList("com.lee.mapper.JDSkuResourceMapper.getBySkuId", skuId).size() == 0) {

                    JDSkuResource jdZnkResource = new JDSkuResource();
                    jdZnkResource.setSkuName(skuName);
                    jdZnkResource.setSkuId(skuId);
                    jdZnkResource.setCreateDt(now);
                    jdZnkResource.setUpdateDt(now);
                    jdZnkResource.setState(1);
                    jdZnkResource.setCategorySeq(catSeq);
                    l.add(jdZnkResource);

                }
                if (l.size() == 10) {
                    session.insert("com.lee.mapper.JDSkuResourceMapper.batchSaveJDZnkResource", l);
                    l.clear();
                }
            }


            if (l.size() > 0) {
                session.insert("com.lee.mapper.JDSkuResourceMapper.batchSaveJDZnkResource", l);
                l.clear();
            }


        session.commit();
        }catch (Exception ex)
        {
            ex.printStackTrace();
            logger.error(ex.getMessage());
        }
        finally {
            if ( null != session ) {

                session.close();
                session = null;
            }
        }
        return true;
    }
}