package com.lee.biz.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Objects;
import com.google.common.base.Optional;
import com.lee.biz.BaseBizInterface;
import com.lee.content.model.JDSkuDetail;

import com.lee.system.exception.MyHttpException;
import com.lee.system.http.HttpRequestContnet;
import com.lee.system.http.HttpRequestHandler;
import com.lee.system.jdbc.JdbcPool;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Author :Lee
 * Since :2018/8/23下午1:41
 * Desc :JDZnkBiz Do What ?
 */
public class JDSkuDetailBiz implements BaseBizInterface {
    private Logger logger = LoggerFactory.getLogger(getClass());

    public boolean execute(String pageResult) {


        //http://p.3.cn/prices/mgets?skuIds=J_11102222371&;type=1 获取jd 商品 价格


        Pattern pattern = Pattern.compile("<div class=\"sku-name\">" + "(.*?)" + "</div>");


        Pattern pattern_1 = Pattern.compile("skuId=(\\d*)&");
        Pattern pattern_2 = Pattern.compile("data-origin=\"(.*?)(\")");


        Matcher matcher = pattern.matcher(pageResult);
        Matcher matcher_1 = pattern_1.matcher(pageResult);
        Matcher matcher_2 = pattern_2.matcher(pageResult);
        String now = new SimpleDateFormat("YYYY-MM-dd hh:mm:ss").format(new Date());

        List l = new ArrayList();
        String price = null;
        String skuId = null;
        String skuName = null;

        String img = null;

        SqlSession session = JdbcPool.buildSqlSession();
        JDSkuDetail jdZnkDetail = new JDSkuDetail();
        if (matcher.find()) {
            skuName = matcher.group(1);
//            price =  matcher.group(4);
//            skuId = matcher.group(3);
//            skuId = skuId.substring(skuId.indexOf("_"),skuId.length());

            skuName = skuName.replaceAll("<img.*/>", "");

            jdZnkDetail.setSkuName(skuName.trim());
        }
//        if ( l.size() > 0 )
//        {
//            session.insert("com.lee.mapper.JDSkuResourceDetailMapper.batchSaveJDZnkResource", l);
//            l.clear();
//        }

        if (matcher_1.find()) {
            skuId = matcher_1.group(1);

            jdZnkDetail.setSkuId(skuId.trim());
        }

        if (matcher_2.find()) {
            img = matcher_2.group(1);

            jdZnkDetail.setSkuImg(img.trim());
        }


        String jsonArr = null;
        if (null != skuId) {


            try {
                jsonArr = new HttpRequestHandler().request(new HttpRequestContnet("http://p.3.cn/prices/mgets?skuIds=J_" + skuId + "&;type=1"));
            } catch (MyHttpException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


            if (Optional.fromNullable(jsonArr).isPresent() && !Objects.equal("", jsonArr)) {

                JSONArray jsonArray = JSONArray.parseArray(jsonArr);

                JSONObject jsonObject = jsonArray.getJSONObject(0);


                price = jsonObject.getString("p");

                jdZnkDetail.setPrice(price.trim());
            }


            logger.info("****** ****** ****** ****** ******");
            logger.info("****** skuId : " + skuId  ) ;
            logger.info("skuName : " + skuName );
            logger.info("price : " + price );
            logger.info("img : " + img );
//            logger.info("currentPage : " + currentPage );
//            logger.info("nextPageUrl : " + nextPageUrl );
//            logger.info("url :" + url );
            logger.info("****** ****** ****** ****** ******");

            jdZnkDetail.setState(1);
            jdZnkDetail.setCreateDt(now);
            jdZnkDetail.setUpdateDt(now);
            jdZnkDetail.setSkuUrl("https://item.jd.com/" + skuId + ".html");

            try {
                session.insert("com.lee.mapper.JDSkuResourceDetailMapper.saveJDSkuDetail", jdZnkDetail);
                session.commit();
            }catch (Exception ex)
            {
                logger.error( " Resource Detail error " +ex.getMessage());
            }
        }
        return true;
    }
}