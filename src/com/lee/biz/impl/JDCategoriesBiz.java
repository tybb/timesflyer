package com.lee.biz.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Objects;
import com.google.common.base.Optional;
import com.lee.biz.BaseBizInterface;
import com.lee.content.model.JDCategories;
import com.lee.content.model.JDSkuDetail;
import com.lee.system.exception.MyHttpException;
import com.lee.system.http.HttpRequestContnet;
import com.lee.system.http.HttpRequestHandler;
import com.lee.system.jdbc.JdbcPool;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Author :Lee
 * Since :2018/8/23下午1:41
 * Desc :JDZnkBiz Do What ?
 */
public class JDCategoriesBiz implements BaseBizInterface {

    private Logger logger = LoggerFactory.getLogger(getClass());

    public boolean execute(String pageResult) {



        //list.jd.com/list.html?cat=670,686,693

//        href="//list.jd.com/list.html?cat=670,686,693" target="_blank">移动硬盘</a>
        Pattern pattern = Pattern.compile("<a href=\"\\/\\/list.jd.com\\/list.html\\?cat=(.*?)\" target=\"_blank\">(.*?)</a>");

        Matcher matcher = pattern.matcher(pageResult);

        String now = new SimpleDateFormat("YYYY-MM-DD hh:mm:ss").format(new Date());


        List l =  new ArrayList();
        String categiorySeq = null;
        String categioryName = null;

        SqlSession session = JdbcPool.buildSqlSession();
        try {
            while (matcher.find()) {


                categiorySeq = matcher.group(1);

                categioryName = matcher.group(2);


                if (categiorySeq.contains("<dd>"))
                    continue;
                logger.info("匹配到京东类别 SEQ:" + categiorySeq + "|| 名称：" + categioryName);

                if (!Objects.equal("1", categiorySeq)
                        && session.selectList("com.lee.mapper.JDCategoriesMapper.getByCategorySeq", categiorySeq).size() == 0) {

                    JDCategories jdCategories = new JDCategories();
                    jdCategories.setCategorySeq(categiorySeq);
                    jdCategories.setCreateDt(now);
                    jdCategories.setUpdateDt(now);
                    jdCategories.setState(1);
                    jdCategories.setCategoriyName(categioryName);
                    l.add(jdCategories);

                }
                if (l.size() == 10) {
                    session.insert("com.lee.mapper.JDCategoriesMapper.batchSaveJDCategories", l);
                    l.clear();
                }
            }


            if (l.size() > 0) {
                session.insert("com.lee.mapper.JDCategoriesMapper.batchSaveJDCategories", l);
                l.clear();
            }

            session.commit();
        }catch (Exception ex)
        {
            ex.printStackTrace();
            logger.error(ex.getMessage());
        }
        finally {
            if ( null != session ) {

                session.close();
                session = null;
            }
        }
        return true;

    }
}