package com.lee.mapper;

import com.lee.content.model.ResourceImg;

import java.util.List;

/**
 * Author :Lee
 * Since :2017/11/8上午12:22
 * Desc :ResourceImgMapper Do What ?
 */
public interface ResourceImgMapper {

        public void batchSaveResourceImg(List<ResourceImg> list);
        public void saveResouceImg(ResourceImg resourceImg);
        public List featchResouceImg(ResourceImg resourceImg);

}
