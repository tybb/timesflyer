package com.lee.mapper;

import com.lee.content.model.JDSkuResource;


import java.util.List;

/**
 * Author :Lee
 * Since :2017/11/8上午12:22
 * Desc :ResourceImgMapper Do What ?
 */
public interface JDSkuResourceMapper {

        public void batchSaveResourceImg(List<JDSkuResource> list);
        public void saveResouceImg(JDSkuResource jdZnkResource);

        public List featchResouceImg(JDSkuResource jdZnkResource);

        public JDSkuResource getBySkuId(String skyId);
}
