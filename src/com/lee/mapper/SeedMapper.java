package com.lee.mapper;

import com.lee.content.model.UrlSeed;

import java.util.List;

/**
 * Author :Lee
 * Since :2017/11/7下午2:39
 * Desc :SeedMapper Do What ?
 */
public interface SeedMapper {

        public void batchSaveUrlSeed(List<UrlSeed> list);
        public void saveUrlSeed(UrlSeed urlSeed);
        public List<UrlSeed> featchList(UrlSeed urlSeed);
        public void updateUrlSeedIsCatch(int id);

}