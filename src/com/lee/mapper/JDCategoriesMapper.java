package com.lee.mapper;

import com.lee.content.model.JDCategories;
import com.lee.content.model.JDSkuResource;

import java.util.List;

/**
 * Author :Lee
 * Since :2017/11/8上午12:22
 * Desc :ResourceImgMapper Do What ?
 */
public interface JDCategoriesMapper {

        public void batchSaveResourceImg(List<JDCategories> list);
        public void saveResouceImg(JDCategories jdZnkResource);

        public List featchResouceImg(JDCategories jdZnkResource);

        public JDCategories getBySkuId(String skyId);
}
