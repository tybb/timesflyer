package com.lee.mapper;

import com.lee.content.model.JDSkuDetail;


import java.util.List;

/**
 * Author :Lee
 * Since :2017/11/8上午12:22
 * Desc :ResourceImgMapper Do What ?
 */
public interface JDSkuDetailMapper {

        public void batchSaveResourceImg(List<JDSkuDetail> list);
        public void saveResouceImg(JDSkuDetail jdZnkResource);

        public List featchResouceImg(JDSkuDetail jdZnkResource);

        public JDSkuDetail getBySkuId(String skyId);
}
