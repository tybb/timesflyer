package com.lee.main;

import com.lee.content.model.SeedList;
import com.lee.system.http.HttpRequestContnet;
import com.lee.system.http.HttpRequestHandler;
import com.lee.system.thread.SearchThread;
import com.lee.task.JDTask;
import com.lee.timer.JDResourceDetailTimer;
import com.lee.timer.JDResourceTimer;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import java.util.List;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by libia on 2016/2/1.
 */
public class EngineStart  {

        public static  void main (String [] args)
        {
//
//            CountDownLatch begSignal = new CountDownLatch(1);
//            CountDownLatch endSignal = new CountDownLatch(5);


            try {



                /**
                 *
                 * 线程池
                 * */
                ExecutorService executorService = Executors.newCachedThreadPool();

//                SeedList.put(new JDTask());


//                "https://www.jd.com/allSort.aspx"  -- 京东全类别



                SearchThread searchThread1 = new SearchThread();
                SearchThread searchThread2 = new SearchThread();
                SearchThread searchThread3 = new SearchThread();
                SearchThread searchThread4 = new SearchThread();
                SearchThread searchThread5 = new SearchThread();


                SearchThread searchThread6 = new SearchThread();
                SearchThread searchThread7 = new SearchThread();
                SearchThread searchThread8 = new SearchThread();
                SearchThread searchThread9 = new SearchThread();
                SearchThread searchThread10 = new SearchThread();



                executorService.submit(searchThread1);
                executorService.submit(searchThread2);
                executorService.submit(searchThread3);
                executorService.submit(searchThread4);
                executorService.submit(searchThread5);

                executorService.submit(searchThread6);
                executorService.submit(searchThread7);
                executorService.submit(searchThread8);
                executorService.submit(searchThread9);
                executorService.submit(searchThread10);

                System.out.println("所有线程已经扫描完成");


                JDResourceTimer.start();
                JDResourceDetailTimer.start();



//                executorService.shutdown();
            }catch (Exception e)
            {
                e.printStackTrace();
            }

        }






}
