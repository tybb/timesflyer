package com.lee.main;


import static java.lang.System.out;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;
import java.util.function.Consumer;
import java.util.function.IntBinaryOperator;
import java.util.stream.Stream;

/**
 * @author
 * @create 2018-02-13 10:30
 **/
public class LambdaTest {


  public static void main(String[] args) {
    String[] names = new String[10];

    int i = 0;
    for (; ; ) {
      if (!(i < 10)) {
        break;
      }

      new Thread(() -> System.out.println("aaaaaaaaaaaa")).start();
      test(() -> {
        System.out.println("wwwww");
      });

      i++;
    }

    Callable<String> c = new Callable<String>() {
      @Override
      public String call() throws Exception {
        return null;
      }
    };

    Callable<Consumer> cc = () -> (t) -> {
      System.out.println("--------------------------Consumer Begin--------------------------");
      System.out.println(t);
      System.out.println("--------------------------Consumer   End--------------------------");
    };
    FutureTask task = new FutureTask(cc);

    new Thread(task).start();
    try {
      Consumer tt = (Consumer) task.get();

      IntBinaryOperator[] intBinaryOperators = new IntBinaryOperator[]{
          (x, y) -> x + y, (x, y) -> x - y, (x, y) -> x * y, (x, y) -> x / y
      };

      String[] sss = new String[]{
          "aaa", "bbb", "ccc"
      };

      Arrays.stream(intBinaryOperators).parallel()
          .forEach(o -> System.out.println(o.applyAsInt(1, 2)));
      Arrays.stream(sss)
          .forEach(System.out::println);
      Arrays.stream(intBinaryOperators).parallel()
          .forEach(System.out::println);


    } catch (InterruptedException e) {
      e.printStackTrace();
    } catch (ExecutionException e) {
      e.printStackTrace();
    }

    int k = 0;
    for (; k < 10; ) {

    }

    System.out.println(" --------------End-------------- ");

  }

  public static void test(A a) {
    a.doSomeThing();
  }

  public Long getCurrentTime() {

    return 0l;
  }

  @FunctionalInterface
  interface A {

    public void doSomeThing();
  }


}
