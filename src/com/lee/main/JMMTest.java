package com.lee.main;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import sun.nio.ch.ThreadPool;

/**
 * @author
 * @create 2018-02-11 10:39
 **/
public class JMMTest {
  private static  int i = 0;
  public static void main(String [] args)
  {
    ExecutorService executorService  =  Executors.newFixedThreadPool(10);


    Runnable r = new Runnable() {
      @Override
      public void run() {
        System.out.println(Thread.currentThread().getName() + "打印" + i);
        i++;
        try {
          Thread.sleep(1000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    };

    for (int k = 0 ;k<20;k++)
      executorService.submit(r);

    while(true){
      //主线程陷入死循环，来观察结果，否则是看不到结果的
    }


  }


}
