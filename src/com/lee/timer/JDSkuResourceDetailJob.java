package com.lee.timer;

import com.google.common.base.Optional;
import com.lee.content.model.JDCategories;
import com.lee.content.model.JDSkuResource;
import com.lee.content.model.SeedList;
import com.lee.system.jdbc.JdbcPool;
import com.lee.task.JDSkuResourceDetailTask;
import com.lee.task.JDSkuResourceTask;
import org.apache.ibatis.session.SqlSession;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Author :Lee
 * Since :2018/8/24下午3:38
 * Desc :JDZnkJob Do What ?
 */
public class JDSkuResourceDetailJob implements Job {


    private Logger logger = LoggerFactory.getLogger(this.getClass());


    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {


        logger.info("***************************** Resource Detail 定时任务开始*****************************");

        SqlSession session = JdbcPool.buildSqlSession();
        List resources = session.selectList("com.lee.mapper.JDSkuResourceMapper.selectNotCatchResource");


        logger.info("***************************** selectNotCatchResource resources ："+ (resources == null ? 0 : resources.size() )+"*****************************");

        if (Optional.fromNullable(resources).isPresent() && resources.size() > 0) {

            for (int i = 0; i < resources.size(); i++) {
                JDSkuResource skuResource = (JDSkuResource) resources.get(i);
                try {


                    //https://item.jd.com/4483022.html
                    SeedList.put(new JDSkuResourceDetailTask("https://item.jd.com/" + skuResource.getSkuId()+".html"));
//                    SeedList.put(new HttpRequestContnet("https://item.jd.com/" + jdSkuResource.getSkuId() + ".html"));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            session.update("com.lee.mapper.JDSkuResourceMapper.updateLastCatch", resources);
            session.commit();
        }

        logger.info("***************************** Resource Detail 定时任务结束*****************************");

    }
}