package com.lee.timer;

import com.google.common.base.Optional;

import com.lee.content.model.JDCategories;
import com.lee.content.model.JDSkuResource;
import com.lee.content.model.SeedList;
import com.lee.system.http.HttpRequestContnet;
import com.lee.system.jdbc.JdbcPool;
import com.lee.task.JDSkuResourceTask;
import org.apache.ibatis.session.SqlSession;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Author :Lee
 * Since :2018/8/24下午3:38
 * Desc :JDZnkJob Do What ?
 */
public class JDSkuResourceJob implements Job {


    private Logger logger = LoggerFactory.getLogger(this.getClass());


    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {


        logger.info("***************************** Resource 定时任务开始*****************************");

        SqlSession session = JdbcPool.buildSqlSession();
        List resources = session.selectList("com.lee.mapper.JDCategoriesMapper.selectNotCatchCategory");


        logger.info("***************************** selectNotCatchCategory categories ："+ (resources == null ? 0 : resources.size() )+"*****************************");

        if (Optional.fromNullable(resources).isPresent() && resources.size() > 0) {

            for (int i = 0; i < resources.size(); i++) {
                JDCategories jdCategories = (JDCategories) resources.get(i);
                try {
                    SeedList.put(new JDSkuResourceTask("http://list.jd.com/list.html?cat=" + jdCategories.getCategorySeq()));
//                    SeedList.put(new HttpRequestContnet("https://item.jd.com/" + jdSkuResource.getSkuId() + ".html"));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            session.update("com.lee.mapper.JDCategoriesMapper.updateLastCatch", resources);
            session.commit();
        }

        logger.info("*****************************定时任务结束*****************************");

    }
}