package com.lee.timer;

import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import java.text.ParseException;

/**
 * Author :Lee
 * Since :2018/8/23下午3:47
 * Desc :JDZnkDetailTimer Do What ?
 */
public class JDResourceDetailTimer {


    public static void start() throws SchedulerException, ParseException {







        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

        //定义一个Trigger
        Trigger trigger = TriggerBuilder.newTrigger().withSchedule(CronScheduleBuilder.cronScheduleNonvalidatedExpression("0/60 * * ? * * *")).withIdentity("JdResourceDetailTrigger","Lee").build();





        //定义一个JobDetail
        JobDetail job = JobBuilder.newJob(JDSkuResourceDetailJob.class) .withIdentity("JdResourceDetail", "Lee")
                .build();


        //加入这个调度
        scheduler.scheduleJob(job, trigger);

        //启动之
        scheduler.start();

        //运行一段时间后关闭
//        Thread.sleep(10000);
//        scheduler.shutdown(true);


    }




}