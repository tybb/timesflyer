package com.lee.timer;

import com.google.common.base.Optional;

import com.lee.content.model.SeedList;
import com.lee.system.http.HttpRequestContnet;
import com.lee.system.jdbc.JdbcPool;
import org.apache.ibatis.session.SqlSession;
import org.quartz.*;
import org.quartz.impl.JobDetailImpl;
import org.quartz.impl.StdSchedulerFactory;
import org.quartz.impl.triggers.CronTriggerImpl;

import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static org.quartz.SimpleScheduleBuilder.simpleSchedule;

/**
 * Author :Lee
 * Since :2018/8/23下午3:47
 * Desc :JDZnkDetailTimer Do What ?
 */
public class JDResourceTimer {


    public static void start() throws SchedulerException, ParseException {







        Scheduler scheduler = StdSchedulerFactory.getDefaultScheduler();

        //定义一个Trigger
        Trigger trigger = TriggerBuilder.newTrigger().withSchedule(CronScheduleBuilder.cronScheduleNonvalidatedExpression("0/60 * * ? * * *")).withIdentity("JdResourceTrigger","Lee").build();





        //定义一个JobDetail
        JobDetail job = JobBuilder.newJob(JDSkuResourceJob.class) .withIdentity("JdResourceJob", "Lee")
                .build();


        //加入这个调度
        scheduler.scheduleJob(job, trigger);

        //启动之
        scheduler.start();

        //运行一段时间后关闭
//        Thread.sleep(10000);
//        scheduler.shutdown(true);


    }




}