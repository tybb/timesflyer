package com.lee.system.jdbc;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;

/**
 * Author :Lee
 * Since :2017/11/9上午11:35
 * Desc :JdbcPool Do What ?
 */
public class JdbcPool {

    private final static String resource = "m.xml";

    private static InputStream inputStream = null;
    private static SqlSessionFactory sqlSessionFactory = null;

//    private static void init() {
//
//
//
//
//    }



    static {
        try {

            inputStream = Resources.getResourceAsStream(resource);
            if (sqlSessionFactory == null)
                sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
//        finally {
//            try {
//
//                inputStream.close();
//                inputStream = null;
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
    }

    public static SqlSession buildSqlSession() {

//        init();
        return sqlSessionFactory.openSession();
    }


}