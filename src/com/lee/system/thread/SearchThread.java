package com.lee.system.thread;


import com.lee.content.model.SeedList;
import com.lee.system.exception.MyHttpException;
import com.lee.system.http.HttpRequestContnet;
import com.lee.system.http.HttpRequestHandler;
import com.lee.task.ITask;


import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.util.Iterator;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by libia on 2016/1/30.
 */
public class SearchThread  implements Runnable {


    private LinkedBlockingQueue<ITask> queue;


    public SearchThread ()
    {

    }



    public void put( List l )
    {
        this.queue.addAll(l);
    }

    public void put( ITask  h )
    {
        this.queue.add(h);
    }






    @Override
    public void run() {
        try {


//            Iterator<HttpRequestContnet> iterator =   this.queue.iterator();

            while ( true )
            {




                this.queue = SeedList.getSeedList();

                /**
                 * 当队列不为空 ， 有任务的时候就开始扒内容
                 */


                synchronized (this) {
                    if (null != this.queue && !this.queue.isEmpty()) {

                        // this.queue.take() 拿取其中的一个任务

//                        HttpRequestContnet h = this.queue.take();

                        ITask task = this.queue.take();
//                        if (null != h) {
//                            String content = new HttpRequestHandler().request(h);
//                            System.out.println("线程 ：" + Thread.currentThread().getName() + "请求" + h.getUrl() + "的返回的内容为:");
//                            System.out.println("==================================================");
//                            System.out.println(content);
//                            System.out.println("==================================================");
//                            System.out.println("请求" + h.getUrl() + "结束:");
//                            new JDZnkDetailBiz().execute(content);
//                            new JDZnkBiz().execute(content);
//                        }

                        task.execute();
                    }
                }

                Thread.sleep(5000);


            }


        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {

        }
    }


}
