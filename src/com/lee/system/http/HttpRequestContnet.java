package com.lee.system.http;

import com.lee.content.model.UrlSeed;

import java.io.Serializable;

/**
 * Created by libia on 2016/1/30.
 */
public class HttpRequestContnet implements Serializable {

    public HttpRequestContnet(String url){
        this.url = url;
    };

    public HttpRequestContnet(UrlSeed urlSeed)
    {
        this.url = urlSeed.getUrl();
        this.urlSeed = urlSeed;
    }

    private String url ;
    private UrlSeed urlSeed;
    private HttpRequestHead httpRequestHead;
    private Object params  ;

    public HttpRequestHead getHttpRequestHead() {
        return httpRequestHead;
    }

    public void setHttpRequestHead(HttpRequestHead httpRequestHead) {
        this.httpRequestHead = httpRequestHead;
    }

    public Object getParams() {
        return params;
    }

    public void setParams(Object params) {
        this.params = params;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public UrlSeed getUrlSeed() {
        return urlSeed;
    }

    public void setUrlSeed(UrlSeed urlSeed) {
        this.urlSeed = urlSeed;
    }
}
