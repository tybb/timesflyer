package com.lee.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Author :Lee
 * Since :2018/8/28下午5:16
 * Desc :ThreadPool Do What ?
 */
public class ThreadPool {


    private static final int size  = 10;
    private static ExecutorService executorService = null;

    static {
        executorService = Executors.newFixedThreadPool(size);
    }


    public static void  execute(Runnable r) {
        executorService.submit(r);
    }

}